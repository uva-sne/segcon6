.SUFFIXES:
.DEFAULT: all


FLAGS = -ggdb -O2 -Wall -pedantic -Werror -fstack-protector-all -D_FORTIFY_SOURCE=2 -pie -fPIE
CFLAGS = -std=gnu99 $(FLAGS)
CPPFLAGS = -std=gnu++0x $(FLAGS)

all: bin/mobility bin/sc6ingress bin/sc6egress bin/seg6snd srh_ingress.o

deploy: all
	echo todo


# Policy tools

DEPS = src/net.cc

bin/sc6egress: src/sc6egress.cc $(DEPS)
	g++ $(CPPFLAGS) $^ -o $@

bin/sc6ingress: src/sc6ingress.cc $(DEPS)
	g++ $(CPPFLAGS) $^ -o $@


# NF

bin/mobility: src/mobility.cc $(DEPS)
	g++ $(CPPFLAGS) $^ -o $@


# Util

bin/seg6snd: src/seg6snd.cc $(DEPS)
	g++ $(CPPFLAGS) $^ -o $@


# BPF

srh_ingress.o: bpf/srh_ingress.c
	./bin/bpf-compile -DMULTI_TENANCY -o $@ -c $^

force_host.o: bpf/force_host.c
	./bin/bpf-compile -DMULTI_TENANCY -o $@ -c $^

r3.o: bpf/redir.c
	./bin/bpf-compile -DTARGET_IFINDEX=3 -o $@ -c $^

test.o: bpf/test.c
	./bin/bpf-compile -o $@ -c $^
