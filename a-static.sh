#!/bin/sh
set -e
set -x

(./ip -6 rule del iif lxcbr0 table 101 || true) 2> /dev/null
./ip -6 rule add iif lxcbr0 table 101

./ip -6 route flush table 101
./ip -6 route add fd0b::dead:2/128 via fdfd:1:a::1 dev ens9 table 101
./ip -6 route add fd0c::dead:2/128 via fdfd:1:a::1 dev ens9 table 101
./ip -6 route add fd0b::/64 encap seg6 mode inline segs fd0b::dead:2 dev ens9 table 101
./ip -6 route add fd0c::/64 encap seg6 mode inline segs fd0c::dead:2 dev ens9 table 101
./ip -6 route show table 101
