#!/bin/sh
set -e
set -x

if [ -n "$V4_TUNNEL" ]
then
	(./ip -4 rule del iif lxcbr0 table 101 || true) 2> /dev/null
	./ip -4 rule add iif lxcbr0 table 101

	./ip -4 route flush table 101
	ip tunnel te101b add ipip6 local fd0a::dead:1 remote fd0b::dead:2
fi


(./ip -6 rule del iif lxcbr0 table 101 || true) 2> /dev/null
./ip -6 rule add iif lxcbr0 table 101

./ip -6 route flush table 101
./ip -6 route add fd0a::dead:2/128 via fdfd:2:b::2 dev ens9 table 101
./ip -6 route add fd0c::dead:2/128 via fdfd:2:b::2 dev ens9 table 101
./ip -6 route add fd0a::/64 encap seg6 mode inline segs fd0a::dead:2 dev ens9 table 101
./ip -6 route add fd0c::/64 encap seg6 mode inline segs fd0c::dead:2 dev ens9 table 101
./ip -6 route show table 101
