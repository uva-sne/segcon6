# segcon6

This repository contains a number of proof-of-concept tools used to implement
segment routing in container networks.

* `bin/srv6-topo` creates virtual topologies
* `bpf/srh_ingress.c` and `bpf/srh_egress.c` contain the eBPF based
  implementations
* `src/sc6ingress.c` and `src/sc6egress.c` contain the userspace
  implementations
* `src/mobility.cc` contains an example network function
* The `a-*` and `b-*` scripts are used to start the container host tools on a
  VM


## TODO

* Do not use this code as-is: not all buffer boundaries are checked correctly.
  Terrible, I know.

* You'll need to set up appropriate MTU routes in the container or increase the
  MTU in the virtual topology

* There are a lot of assumptions baked in that the SRH is the first extension
  header. Build a general metadata structure with proper offsets.

* Decapsulating IP-in-IP
    * IPv4 and IPv6 tunnel interfaces

* Don't assume static prefixes for overlays

* Implement a nice policy configuration file format


## Copyright

This copyright notice pertains to all files in this repository unless stated
otherwise.  This project contains work inspired by the Cilium project.
See also https://github.com/cilium/cilium

Copyright (C) 2017 Ben de Graaff <ben.degraaff@os3.nl>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
