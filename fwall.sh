#!/bin/sh
set -x
ip6tables -t mangle -F PREROUTING
ip6tables -t mangle -A PREROUTING -i veth269LM8 -j MARK --set-mark 666
ip6tables -t mangle -A PREROUTING -i lxcbr0 -j MARK --set-mark 666
ip -6 rule del fwmark 666 table 1000 2> /dev/null
ip -6 rule add fwmark 666 table 1000
ip -6 route del default dev tun0 table 1000 2> /dev/null
ip -6 route add default dev tun0 table 1000
