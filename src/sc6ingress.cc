/* Policy enforcing ingress router for multi-tenancy container networks
 *
 * Arguments, can be repeated:
 * - Tenant SID
 * - Zero or more "iface/ip" pairs, mapping the last segment to the container
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip6.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <linux/if_packet.h>
//#include <linux/ipv6.h>
#include <arpa/inet.h>

#include <unordered_map>
#include <map>
#include <array>
#include <vector>
#include <iostream>

#include "net.h"
//#define VERBOSE
#include "util.h"

// <container><overlay sid>
typedef std::array<uint8_t, 32> sc_key;

struct b32hash {
	std::size_t operator()(const sc_key& a) const {
		std::size_t h = 0;
		for (auto e : a) {
			h ^= std::hash<int>{}(e) + 0x9e3779b9 + (h << 6) + (h >> 2);
		}
		return h;
	}
};

struct sc_container {
	uint32_t tenant;
	uint32_t ifindex;
	uint8_t mac[6];
};

static std::unordered_map<sc_key, struct sc_container, b32hash> sids = {};
//static std::vector<struct tenant_config> tenants = {};

#ifndef NO_SENDER_FD
static int senderfd = -1;
#endif

static const uint8_t broadcast_mac[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
static int iface_mode = 1 ? IFF_TAP : IFF_TUN;


/* Ingress: forward to correct bridge/iface
 *
 * Config: map[ip6] => iface
 * + TLV restrictions
 */
static int process_ingress(struct sk_buff *s)
{
	// ETH_HLEN + ipv6hdr + shr
	if (s->etype != 0x86DD || s->len < 62) {
		//fprintf(stderr, "discard etype %04x len %zu\n", s->etype, s->len);
		return 0;
	}

	struct ip6_hdr *ip6 = (struct ip6_hdr *) (s->data + ETH_HLEN);
	if (is_ignored_v6addr(reinterpret_cast<uint8_t *>(&ip6->ip6_dst)))
		return 0;

#ifdef VERBOSE
	printv6(&ip6->ip6_src);
	verbosef(" > ");
	printv6(&ip6->ip6_dst);
	verbosef("\n");
#endif

	uint8_t next_proto;
	size_t ip6_hlen = 40;
	sc_key key;

	if (ip6->ip6_nxt == 43) {
		struct ip6_srh *srh = ipv6_srh_valid(s->data + 54, s->len - 54);

		if (srh == NULL) {
			verbosef("Drop packet with invalid SRH\n");
			return 0;
		} else if (srh->segsleft != 1) {
			// TODO: support == 0
			verbosef("Drop packet with invalid SegsLeft %u\n", srh->segsleft);
			return 0;
		}

		memcpy(key.data(), reinterpret_cast<uint8_t *>(srh) + 8, 16);
		memcpy(key.data() + 16, &ip6->ip6_dst, 16);
		next_proto = srh->nexthdr;
#ifndef KEEP_SRH
		ip6->ip6_nxt = next_proto;
		size_t sz = srh->hdrlen * 8 + 8;
		buf_shrink_space(s, ETH_HLEN + 40, sz);

		ip6->ip6_plen = ntohs(htons(ip6->ip6_plen) - sz);
#else
		ip6_hlen += srh->hdrlen * 8 + 8;
		srh->segsleft = 0;
#endif
	} else {
		memcpy(key.data(), &ip6->ip6_dst, 16);
		memset(key.data() + 16, 0, 16);
		next_proto = ip6->ip6_nxt;
	}

#ifdef VERBOSE
	verbosef("Key: ");
	for (int i = 0; i < 32; i++)
		verbosef("%02x", key[i]);
	verbosef("\n");
	verbosef("(");
	printv6(key.data());
	verbosef(", ");
	printv6(key.data()+16);
	verbosef(")\n");
#endif

	// Can be global or local
	auto v = sids.find(key);
	if (v == sids.end()) {
		verbosef("Element not found\n");
		return 0;
	}

	uint8_t *dst = key.data();
	struct sc_container *c = &v->second;

#ifdef VERBOSE
	verbosef("Tenant %d  Container ", c->tenant);
	printv6(dst);
	verbosef("\n");
#endif

	memcpy(s->data, c->mac, 6);

	if (next_proto == 4) {
		verbosef("V4 decap\n");
		s->data[12] = 0x08;
		s->data[13] = 0x00;
		buf_shrink_space(s, ETH_HLEN, ip6_hlen);
		//hexdump(s->data, s->len);
	} else {
		memcpy(&ip6->ip6_dst, dst, 16);
	}


#ifdef NO_SENDER_FD
	return write(t->second, s->data, s->len);
#else
	struct sockaddr_ll sa = {0};
	sa.sll_ifindex = c->ifindex;
	sa.sll_halen = ETH_ALEN;
	memset(sa.sll_addr, 0xFF, sizeof(sa.sll_addr));
	return sendto(senderfd, s->data, s->len, 0,
			(struct sockaddr *) &sa, sizeof(sa));
#endif
}



static int is_iface_link(const char *s)
{
	while (*s) {
		if (*s == '/')
			return 1;
		s++;
	}
	return 0;
}


int main(int argc, char **argv)
{
	const char *indevice;

	if (argc < 3) {
		fprintf(stderr, "Usage: %s\n", argv[0]);
		return 1;
	}

	indevice = argv[1];
#ifdef DO_ME_A_TUNNEL
	int ifd = tun_alloc(IFF_TUN, NULL);
#else
	int ifd = iface_raw(indevice);
#endif

	if (ifd < 0) abort();
#ifndef NO_SENDER_FD
	senderfd = iface_raw("lxcbr0");
	if (senderfd < 0) abort();
#endif

	uint32_t tid = 0;
	sc_key key;
	for (int i = 2; i < argc; i++) {
		fprintf(stderr, "%d %s\n", i, argv[i]);
		uint8_t a[16];
		if (tid == 0 || !is_iface_link(argv[i])) {
			verbosef("Add TID %s\n", argv[i]);
			inet_pton(AF_INET6, argv[i], a);
			memcpy(key.data() + 16, a, 16);
			tid += 1;
		/*} else if (argv[i][0] == '@') {*/
		} else {
			char *ifname = argv[i];
			char *addr = NULL;
			char *mac = NULL;
			fprintf(stderr, "Add if %s to %d\n", argv[i], tid - 1);
			char *ptr = ifname;
			for (; *ptr;) {
				if (*ptr == '/') {
					*ptr = 0;
					addr = ptr + 1;
					ptr++;
					break;
				}
				ptr++;
			}
			for (; *ptr;) {
				if (*ptr == '/') {
					*ptr = 0;
					mac = ptr + 1;
					break;
				}
				ptr++;
			}
			if (!addr || !mac) abort();

			inet_pton(AF_INET6, addr, a);
			memcpy(key.data(), a, 16);
#ifdef NO_SENDER_FD
			uint32_t ifindex = iface_raw(ifname);
#else
			uint32_t ifindex = iface_index(ifd, ifname);
#endif
			sc_container c = {tid, ifindex, {}};
			parse_mac(c.mac, mac);
			sids[key] = c;
		}
	}

	struct iface_cfg ic = {ifd,
		(ssize_t) (iface_mode == IFF_TUN ? 4 : 14),
		(size_t) (iface_mode == IFF_TUN ? ETH_HLEN - 4 : 0)};
	process_packets(ic, process_ingress);
	close(ifd);
	return 1;
}
