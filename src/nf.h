#pragma once

#define PKT_DROP 0
#define PKT_OK 1

static net_func_t the_func = NULL;
static int sendfd = 0;

//#define VERBOSE
#include "util.h"

static int process_nf(struct sk_buff *s)
{
	if (the_func(s) == PKT_DROP) {
		//verbosef("Dropped\n");
		return 0;
	}
	//verbosef("Forwarded\n");

	// Invert L2 MAC for hairpin
	uint8_t mac[6];
	memcpy(mac, s->data, 6);
	memcpy(s->data, s->data + 6, 6);
	memcpy(s->data + 6, mac, 6);

	return write(sendfd, s->data, s->len);
}


static int netfunc(net_func_t nf, const char *script, const char *iface)
{
#if 0
	int ifd = tun_alloc(IFF_TAP, script);
#else
	int ifd = iface_raw(iface);
#endif
	if (ifd < 0) return 1;

	sendfd = iface_raw(iface);
	if (sendfd < 0) return 1;

	struct iface_cfg ic = {ifd, 14, 0};

	the_func = nf;
	printf("Processing packets...\n");
	process_packets(ic, process_nf);
	return 0;
}
