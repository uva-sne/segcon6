#pragma once

#define BUF_SIZE 5012

/* Simple socket buffer emulation
 */
struct sk_buff {
	uint8_t *data;
	size_t len;
	size_t space;
	uint16_t etype;
};

int buf_reserve_space(struct sk_buff *s, uint16_t at, uint16_t size);
int buf_shrink_space(struct sk_buff *s, uint16_t at, uint16_t size);

#define ETH_HLEN 14
#define IPV6_HLEN 40
#define IPV6_LENGTH_OFFSET 4
#define ROUTING_SRH 4
#define NEXTHDR_ROUTING 43

#define MUT_SRH_HERE 1 /* Add before next segment */
#define MUT_SRH_END 2 /* Override destination */

struct iface_cfg {
	int fd;
	ssize_t min_read;
	size_t offset;
};

typedef int (*net_func_t)(struct sk_buff *buf);
void process_packets(struct iface_cfg ic, net_func_t cb);
void process_packets_tap(struct iface_cfg ic, net_func_t cb);

static __attribute__((unused)) inline int is_ignored_v6addr(const uint8_t *addr)
{
	return addr[0] == 0xff || (addr[0] == 0xfe && addr[1] == 0x80);
}

void parse_mac(uint8_t *mac, const char *s);
int iface_mac(uint8_t *mac, const char *ifname, int fd);
int iface_maci(uint8_t *mac, int ifindex, int fd);

int iface_raw(const char *iface_name);
int iface_index(int fd, const char *iface_name);
int tun_alloc(int flags, const char *run_script);

struct ip6_srh {
	uint8_t nexthdr;
	uint8_t hdrlen;
	uint8_t type;
	uint8_t segsleft;
	uint8_t last;
	uint8_t flags;
	uint16_t tag;
} __attribute__((packed));


#define SRH_META_TLV 0x42

struct srh_meta {
	size_t bufsz;
	size_t pktsz;

	uint8_t *segments;
	uint8_t num_segs;

	size_t metasz;
	uint8_t *meta;
};


/* Caller must ensure SRH would fit
 */
void ipv6_srh_add(struct sk_buff *buf,
	const uint8_t *segments, uint8_t num_segs);

void ipv6_srh_extend(struct sk_buff *buf,
	const uint8_t *segs, uint8_t num_segs, uint8_t flags);

void ipv6_srh_store(uint8_t *buf,
		uint8_t nexthdr,
		const uint8_t *segments, uint8_t num_segs,
		uint8_t *tlv_block, size_t tlv_len);

struct ip6_srh *ipv6_srh_valid(uint8_t *ptr, size_t tail_space);
int ipv6_encap(struct sk_buff *buf, struct ip6_hdr *ip6);

/* Helper that also swaps out destination address as final segment,
 * segment buffer must have space for this
 */
#define IPV6_SRH_DST(buf, segments, num) \
	{ \
		struct ip6_hdr *ip6 = (struct ip6_hdr *) ((buf)->data + ETH_HLEN); \
		memcpy(segments, (uint8_t *) &ip6->ip6_dst, 16); \
		ipv6_srh_add(buf, segments, num + 1); \
		memcpy((uint8_t *) &ip6->ip6_dst, segments + num * 16, 16); \
	}

void l2_add(uint8_t *buf,
		const uint8_t *dst, const uint8_t *src, uint16_t etype);

