/* Egress SRH policy
 *
 * * Receiver packets based on tun/tap [or AF_PACKET]
 * * Send out packets
 *
 * Future work:
 * - Conditionally assign segment list
 */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>

#include <unordered_map>

#include "net.h"
//#define VERBOSE
#include "util.h"

#define BUF_SIZE 5012

// TODO: various sizes
typedef uint64_t pfx_key;
typedef struct { uint8_t a[16]; } segment_t;

// TODO
static const uint32_t tenant_id = 1;


struct egress {
	uint8_t tunnel_src[16];
	uint8_t src_mac[6];
	int out;
};


struct pfx_value {
	int is_overlay;
	uint8_t tunnel_pfx[8];
	segment_t segments[3];
	uint8_t num_segs;
};

static std::unordered_map<pfx_key, struct pfx_value> prefixes = {};

#define FORCE_DEST

static const uint8_t broadcast_mac[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
static char *run_script = NULL;
static int iface_mode = 0 ? IFF_TAP : IFF_TUN;
static struct egress egress_policy = {0};


// ------


/* Egress: forward packet to out-interface
 *
 * Config: srh-policy, L2 addresses
 */
static int process_egress(struct egress *e, struct sk_buff *s)
{
	pfx_key tunnel_key;
	uint8_t daddr[16];
	uint8_t ovaddr[16];

	if (s->etype == 0x0800) {
		// Use the dest /24 as key
		struct iphdr *ip = (struct iphdr *) (s->data + ETH_HLEN);
		tunnel_key = ((uint64_t) ip->daddr) << 40;
		memset(daddr, 0, 12);
		uint32_t *p = (uint32_t *) daddr;
		p[0] = p[1] = p[2] = 0;
		p[3] = ip->daddr;
	} else if (s->etype == 0x86dd) {
		struct ip6_hdr *ip6 = (struct ip6_hdr*) (s->data + ETH_HLEN);
		if (is_ignored_v6addr((uint8_t *) &ip6->ip6_dst))
			return 0;
		tunnel_key = reinterpret_cast<uint64_t *>(&ip6->ip6_dst)[0];
		memcpy(daddr, &ip6->ip6_dst, 16);
	} else {
		return 0;
	}

	struct ip6_hdr *ip6 = (struct ip6_hdr*) (s->data + ETH_HLEN);
	uint8_t nxt;

	verbosef("TK %016zx\n", tunnel_key);
	int is_overlay = 0;
	auto v = prefixes.find(tunnel_key);
	segment_t *segs = NULL;
	uint8_t num_segs = 0;

	if (v == prefixes.end()) {
		verbosef("Direct send\n");
		// Direct send
	} else {
		is_overlay = v->second.is_overlay;
		segs = v->second.segments;
		num_segs = v->second.num_segs;
		verbosef("Overlay? %d\n", is_overlay);
		if (is_overlay) {
			memcpy(ovaddr, v->second.tunnel_pfx, 8);
			uint32_t *p = (uint32_t *) ovaddr;
			p[2] = 0;
			p[3] = htonl((0xdead << 16) + tenant_id);
		}
	}

#ifdef VERBOSE
	verbosef("dst ");
	printv6(daddr);
	verbosef(" ovaddr ");
	printv6(ovaddr);
	verbosef("\n");
#endif

	int rsegs = 1 + (is_overlay ? 1 : 0);
	int slen = 8 + (num_segs + rsegs) * 16;

	if (s->etype == 0x0800) {
		// Encap payload
		s->etype = 0x86DD;

		// IP payload to end
		buf_reserve_space(s, ETH_HLEN, 40 + slen);

		nxt = 4;
		ip6->ip6_vfc = 96;
		ip6->ip6_hlim = 64;
		s->len += 40 + slen;
		ip6->ip6_plen = htons(s->len - 40 - ETH_HLEN);
		memcpy(&ip6->ip6_src, e->tunnel_src, 16);
		//memcpy(&ip6->ip6_dst, is_overlay ? ovaddr : daddr, 16);
	} else {
		// Alloc SRH space
		buf_reserve_space(s, ETH_HLEN + 40, slen);
		ip6->ip6_plen = htons(s->len - 40 - ETH_HLEN);
		nxt = ip6->ip6_nxt;

		// XXX or first segment
		//if (is_overlay)
		//	memcpy(&ip6->ip6_dst, ovaddr, 16);
	}

	ip6->ip6_nxt = IPPROTO_ROUTING;
	ipv6_srh_store(s->data + ETH_HLEN + 40, nxt, NULL, num_segs + rsegs, NULL, 0);
	size_t offz = ETH_HLEN + 40 + 8;
	// Copy that floppy
	memcpy(s->data + offz, daddr, 16);
	offz += 16;
	if (is_overlay) {
		memcpy(s->data + offz, ovaddr, 16);
		offz += 16;
	}
	if (num_segs) {
		memcpy(s->data + offz, segs, 16 * num_segs);
	}

	// First segment
	memcpy(&ip6->ip6_dst,
			s->data + ETH_HLEN + 48 + (num_segs + rsegs - 1) * 16, 16);

	l2_add(s->data, broadcast_mac, e->src_mac, s->etype);
	//hexdump(s->data, s->len);
	return write(e->out, s->data, s->len);
}

// ------


/* Copy packets between interfaces
 */
static void process_packets(int tun, struct egress *ep)
{
	uint8_t buffer[BUF_SIZE];
	size_t pktbufsz = sizeof(buffer);
	uint8_t *pktbuf = buffer;
	ssize_t min_read = iface_mode == IFF_TUN ? 4 : 14;
	size_t offset = iface_mode == IFF_TUN ? ETH_HLEN - 4 : 0;

	while (1) {
		ssize_t nread = read(tun, pktbuf + offset, pktbufsz - offset);
		if (nread < min_read) {
			  perror("small read");
			  break;
		}

		uint16_t etype = ntohs(((uint16_t *) pktbuf)[6]);

		if (iface_mode == IFF_TUN)
			nread += ETH_HLEN - 4;

		struct sk_buff s = {buffer, (size_t) nread, BUF_SIZE, etype};

		if (process_egress(ep, &s) < 0) {
			// MTU issues
			if (errno == EMSGSIZE)
				continue;
			perror("process_egress");
			break;
		}
	}
}


int main(int argc, char **argv)
{
	const char *outdevice;
	if (argc < 4) {
		fprintf(stderr, "Usage: %s script out-if tun-src\n", argv[0]);
		return 1;
	}

	run_script = argv[1];
	outdevice = argv[2];
	inet_pton(AF_INET6, argv[3], egress_policy.tunnel_src);

	// TODO: load container <-> tenant map
	// TODO: load prefix map
	prefixes[0x0000000000000afdUL] = {1, {0xfd, 0x0a, 0, 0, 0, 0, 0, 0}, {}, 0};
	prefixes[0x0000000000000bfdUL] = {1, {0xfd, 0x0b, 0, 0, 0, 0, 0, 0}, {}, 0};
	prefixes[0x05000a0000000000UL] = {1, {0xfd, 0x0a, 0, 0, 0, 0, 0, 0}, {}, 0};
	prefixes[0x04000a0000000000UL] = {1, {0xfd, 0x0b, 0, 0, 0, 0, 0, 0}, {}, 0};

	// Opaque function
	prefixes[0x000000000000fffdUL] = {0, {}, {
		{0xfd, 0xcc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}}, 1};

	int td = tun_alloc(iface_mode, run_script);
	if (td < 0) {
		perror("tun_alloc");
		return 1;
	}

	int fd = iface_raw(outdevice);
	if (fd < 0) {
		return 1;
	}

	egress_policy.out = fd;
	iface_mac(egress_policy.src_mac, outdevice, fd);

	process_packets(td, &egress_policy);
	close(td);
	close(fd);
	return 1;
}
