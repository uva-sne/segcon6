/* Opaque container address routing
 */
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <linux/if_tun.h>
#include <netinet/ip6.h>
#include <arpa/inet.h>

#define VERBOSE
#include "net.h"
#include "nf.h"
#include "util.h"

#include <unordered_map>


static std::unordered_map<__uint128_t, __uint128_t> mobility = {};


static int
lookup_hop(const uint8_t *addr, uint8_t *segments)
{
	auto c = mobility.find(ip6_as_int128(addr));
	if (c != mobility.end()) {
		memcpy(segments, &c->second, 16);
		return 1;
	}
	return 0;
}


static int
seg6_mobility(struct sk_buff *buf)
{
	uint8_t segment[16];
	struct ip6_hdr *ip = (struct ip6_hdr *) (buf->data + ETH_HLEN);
	struct ip6_srh *srh;
	uint8_t *addr;

	if (ip->ip6_nxt != NEXTHDR_ROUTING) {
		return PKT_DROP;
	}

	srh = ipv6_srh_valid(buf->data + ETH_HLEN + 40, buf->len - ETH_HLEN - 40);
	if (!srh || srh->segsleft == 0) {
		return PKT_DROP;
	}

	addr = buf->data + ETH_HLEN + 48 + (srh->segsleft - 1) * 16;

	if (!lookup_hop(addr, segment)) {
		return PKT_DROP;
	}

	ipv6_srh_extend(buf, segment, 1, MUT_SRH_HERE);
	memcpy(&ip->ip6_dst, segment, 16);
	return PKT_OK;
}


static void
store_map(const char *con, const char *loc)
{
	uint8_t a[16], b[16];
	inet_pton(AF_INET6, con, a);
	inet_pton(AF_INET6, loc, b);
	mobility[ip6_as_int128(a)] = ip6_as_int128(b);
}


int main(int argc, char **argv)
{
	// TODO: Fixed table for now
	store_map("fdff::1:2:3:4", "fd0a::1");
	store_map("fdff::a:b:c:d", "fd0b::1");
	return netfunc(seg6_mobility, "./nf-route", "br3-cc");
}
