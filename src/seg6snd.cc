#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip6.h>
#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <linux/if_packet.h>

#include "net.h"
#include "util.h"

struct packet_info {
	int encap;
	uint8_t encap_src[16];
	uint8_t encap_dst[16];

	uint8_t src[16];
	uint8_t dst[16];

	uint8_t mac_src[6];
	uint8_t mac_dst[6];

	uint8_t num_segs;
	uint8_t *segs;
};

static const uint8_t broadcast_mac[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

static void icmpv6_csum(uint16_t *addr,
	uint8_t *src, uint8_t *dst, uint16_t plen, uint8_t nh,
	uint8_t *data, size_t sz)
{
   uint32_t   total;
   uint16_t * ptr;
   int        words;

   total = 0;

   // Pseudo header
   for (int i = 0; i < 16; i++) {
	   total += src[i] + dst[i];
   }
   total += plen + nh;

   ptr   = (uint16_t *) data;
   words = (sz + 1) / 2;
   while (words--) total += *ptr++;
   while (total & 0xffff0000) total = (total >> 16) + (total & 0xffff);

   *addr = ~ ((uint16_t) total);
   printf("csum %04x\n", *addr);
}



static void write_packet(int out, struct packet_info *pi)
{
	size_t pktsz = 64 + 40;

	/* ICMP packet */
	uint8_t buffer[5012] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00,

		0x60, 0x07, 0x3a, 0x13, 0x00, 0x40, 0x3a, 0x3d,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

		0x80, 0x00, 0x52, 0xc5, 0x17, 0x5d, 0x00, 0x01, 0x2d, 0xe6,
		0x4b, 0x59, 0x00, 0x00, 0x00, 0x00, 0xde, 0x4a, 0x05, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15,
		0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
		0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29,
		0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33,
		0x34, 0x35, 0x36, 0x37,
	};
	// TODO: fix checksum

	memcpy(buffer + ETH_HLEN + 40 - 32, pi->src, 16);
	memcpy(buffer + ETH_HLEN + 40 - 16, pi->dst, 16);
	uint16_t plen = 64;
	icmpv6_csum((uint16_t *) (buffer + ETH_HLEN + 2),
			pi->src,
			pi->num_segs ? pi->segs : pi->dst,
			htons(plen), 0x3a,
			buffer + ETH_HLEN + 40, 64);

	l2_add(buffer, pi->mac_dst, pi->mac_src, 0x86DD);

	struct sk_buff s = {buffer, pktsz + ETH_HLEN, 5012};

	if (pi->encap) {
		struct ip6_hdr outer;
		memcpy(&outer, buffer + ETH_HLEN, 40);
		memcpy(&outer.ip6_src, pi->encap_src, 16);
		memcpy(&outer.ip6_dst, pi->encap_dst, 16);
		ipv6_encap(&s, &outer);
	}

	if (pi->num_segs) {
		ipv6_srh_add(&s, pi->segs, pi->num_segs);
	}

	hexdump(s.data, s.len);

	// TODO discard reads to fd
	if (write(out, s.data, s.len) < 0) {
		perror("write");
	}
}


/* TODO arguments:
 * route
 * output device
 * source address
 * src mac?
 * dst mac
 */
int main(int argc, char **argv)
{
	const char *outdevice;
	struct ifreq req = {0};

	if (argc < 2) {
		fprintf(stderr, "Usage: %s out-if [dmac ...] [encap src dst] [dst] [src] [segs]\n", argv[0]);
		return 1;
	}

	outdevice = argv[1];

	int k = -4;
	struct packet_info pi = {0};
	memcpy(pi.mac_dst, broadcast_mac, 6);

	for (int i = 2; i < argc; i++) {
		if (k == -4) {
			if (strcmp(argv[i], "dmac") == 0) {
				k++;
				continue;
			}
			k = 0;
		}

		if (k == -3) {
			parse_mac(pi.mac_dst, argv[i]);
			k = 0;
			continue;
		} else if (k == 0) {
			if (strcmp(argv[i], "encap") == 0) {
				k = 1;
				pi.encap = 1;
				continue;
			}
			k = 10;
		}
		if (k == 1) {
			inet_pton(AF_INET6, argv[i], pi.encap_src);
			k++;
		} else if (k == 2) {
			inet_pton(AF_INET6, argv[i], pi.encap_dst);
			k = 10;
		} else if (k == 10) {
			inet_pton(AF_INET6, argv[i], pi.src);
			k++;
		} else if (k == 11) {
			inet_pton(AF_INET6, argv[i], pi.dst);
			k++;
		} else {
			pi.num_segs++;
			size_t sz = 16 * pi.num_segs;
			if (pi.segs == NULL) {
				pi.segs = (uint8_t *) malloc(sz);
			} else {
				pi.segs = (uint8_t *) realloc(pi.segs, sz);
			}
			if (pi.segs == NULL) abort();
			inet_pton(AF_INET6, argv[i], pi.segs + 16 * (pi.num_segs - 1));
		}
	}

	//int fd = socket(PF_PACKET, SOCK_RAW, 0);
	int fd = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW);
	if (fd < 0) {
		perror("socket");
		return 1;
	}

	snprintf(req.ifr_name, sizeof(req.ifr_name), "%s", outdevice);
	if (ioctl(fd, SIOCGIFINDEX, &req) < 0) {
		perror("ioctl");
		return 1;
	}

	iface_mac(pi.mac_src, outdevice, fd);
	if (1) {
		fprintf(stderr, "---\n    From: ");
		printv6(pi.src);
		fprintf(stderr, " (");
		printmac(pi.mac_src);
		fprintf(stderr, ")\n      To: ");
		printv6(pi.dst);
		fprintf(stderr, " (");
		printmac(pi.mac_dst);
		fprintf(stderr, ")");
		if (pi.encap) {
			fprintf(stderr, "\nEncapSrc: ");
			printv6(pi.encap_src);
			fprintf(stderr, "\nEncapDst: ");
			printv6(pi.encap_dst);
		}
		for (int i = 0; i < pi.num_segs; i++) {
			fprintf(stderr, "\n  Seg[%d]: ", i);
			printv6(pi.segs + 16 * i);
			if (i == pi.num_segs - 1) fprintf(stderr, " [first]");
		}
		fprintf(stderr, "\n---\n");
	}

	struct sockaddr_ll addr = {PF_PACKET, 0, req.ifr_ifindex};

	if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}

	write_packet(fd, &pi);
	close(fd);
	return 1;
}
