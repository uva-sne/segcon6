/* Network helpers
 */
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip6.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/if_tun.h>

#include "net.h"
#include "util.h"


int buf_reserve_space(struct sk_buff *s, uint16_t at, uint16_t gap)
{
	size_t final_size = s->len + gap;

	if (at > s->len || final_size > s->space)
		return -1;

	memmove(s->data + at + gap,
			s->data + at,
			s->len - at);
	s->len = final_size;
	return 0;
}


int buf_shrink_space(struct sk_buff *s, uint16_t at, uint16_t gap)
{
	if (at > s->len || gap > s->len)
		return -1;

	size_t final_size = s->len - gap;

	memmove(s->data + at,
			s->data + at + gap,
			s->len - at - gap);
	s->len = final_size;
	return 0;
}

static inline
int buf_fill(struct sk_buff *s, uint16_t at, const void *buf, uint16_t sz)
{
	size_t final_size = at + sz;
	if (at > s->len || final_size > s->space)
		return -1;
	/* Allow fill to extend slightly, this will probably lead to bugs */
	else if (final_size > s->len)
		s->len = final_size;
	memcpy(s->data + at, buf, sz);
	return 0;
}

// ----

void parse_mac(uint8_t *mac, const char *s)
{
	uint32_t buf[6];
	sscanf(s, "%x:%x:%x:%x:%x:%x:", &buf[0], &buf[1], &buf[2], &buf[3],
			&buf[4], &buf[5]);
	for (int i = 0; i < 6; i++)
		mac[i] = buf[i];
}


int iface_mac(uint8_t *mac, const char *ifname, int fd)
{
	struct ifreq ifr = {0};
	snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", ifname);

	if (ioctl(fd, SIOCGIFHWADDR, (char *) &ifr) < 0) {
		return -1;
	}

	memcpy(mac, ifr.ifr_ifru.ifru_hwaddr.sa_data, 6);
	return 0;
}

int iface_maci(uint8_t *mac, int ifindex, int fd)
{
	struct ifreq ifr = {0};
	ifr.ifr_ifindex = ifindex;

	if (ioctl(fd, SIOCGIFHWADDR, (char *) &ifr) < 0) {
		perror("ioctl(SIOCGIFHWADDR)");
		return -1;
	}

	memcpy(mac, ifr.ifr_ifru.ifru_hwaddr.sa_data, 6);
	return 0;
}


int iface_index(int fd, const char *iface_name)
{
	struct ifreq req = {0};

	snprintf(req.ifr_name, sizeof(req.ifr_name), "%s", iface_name);
	if (ioctl(fd, SIOCGIFINDEX, &req) < 0) {
		perror("ioctl");
		return -1;
	}

	return req.ifr_ifindex;
}

int iface_raw(const char *iface_name)
{
	int fd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (fd < 0) {
		perror("socket");
		return -1;
	}

	if (iface_name) {
		//fprintf(stderr, "bind to %s\n", iface_name);
		do {
			int index = iface_index(fd, iface_name);
			if (index < 0)
				break;

			struct sockaddr_ll addr = {0};
			addr.sll_family = AF_PACKET;
			addr.sll_ifindex = index;
			addr.sll_pkttype = PACKET_HOST;

			if (bind(fd, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
				perror("bind");
				break;
			}

			return fd;
		} while (0);
	} else {
		return fd;
	}

	close(fd);
	return -1;
}


int tun_alloc(int flags, const char *run_script)
{
	struct ifreq ifr = {0};
	int fd, err;

	if ((fd = open("/dev/net/tun", O_RDWR)) < 0) {
		perror("Opening /dev/net/tun");
		return fd;
	}

	if (flags == IFF_TAP) {
		flags |= IFF_NO_PI;
	}

	ifr.ifr_flags = flags;
	if ((err = ioctl(fd, TUNSETIFF, (void *)&ifr)) < 0) {
		perror("ioctl(TUNSETIFF)");
		goto bail;
	}

	printf("ifname: %s\n", ifr.ifr_name);
	if ((err = ioctl(fd, TUNSETNOCSUM, 1)) < 0) {
		perror("ioctl(TUNSETNOCSUM)");
		goto bail;
	}

	// Lame, but easier than implementing netlink stuff..
	callf("sysctl -w net.ipv6.conf.%s.autoconf=0", ifr.ifr_name);
	callf("ip link set %s addrgenmode none", ifr.ifr_name);
	callf("ip link set %s up", ifr.ifr_name);

	if (run_script && *run_script) {
		callf("%s %s", run_script, ifr.ifr_name);
	}

	return fd;

bail:
	close(fd);
	return err;
}


/* Read packets from interface
 */
void process_packets(struct iface_cfg ic, net_func_t cb)
{
	uint8_t buffer[BUF_SIZE];
	size_t pktbufsz = sizeof(buffer);
	uint8_t *pktbuf = buffer;
	//ssize_t min_read = iface_mode == IFF_TUN ? 4 : 14;
	//size_t offset = iface_mode == IFF_TUN ? ETH_HLEN - 4 : 0;

	while (1) {
		struct sockaddr_ll from;
		socklen_t fromlen = sizeof(from);
		ssize_t nread = recvfrom(ic.fd, pktbuf + ic.offset, pktbufsz - ic.offset,
			MSG_TRUNC, (struct sockaddr *) &from, &fromlen);
		if (nread < 0) {
			perror("recvfrom");
			break;
		} else if (nread < ic.min_read) {
			continue;
		} else if (from.sll_pkttype == PACKET_OUTGOING) {
			continue;
		}
		uint16_t etype = ntohs(((uint16_t *) pktbuf)[6]);
		//if (iface_mode == IFF_TUN)
		//	nread += ETH_HLEN - 4;
		nread += ic.offset;
		struct sk_buff s = {buffer, (size_t) nread, BUF_SIZE, etype};
		if (cb(&s) < 0) {
			perror("cb");
			break;
		}
	}
}

void process_packets_tap(struct iface_cfg ic, net_func_t cb)
{
	uint8_t buffer[BUF_SIZE];
	size_t pktbufsz = sizeof(buffer);
	uint8_t *pktbuf = buffer;

	while (1) {
		ssize_t nread = read(ic.fd, pktbuf + ic.offset, pktbufsz - ic.offset);
		if (nread < 0) {
			perror("recvfrom");
			break;
		} else if (nread < ic.min_read) {
#if 0
			perror("small read");
			break;
#else
			continue;
#endif
		}
		uint16_t etype = ntohs(((uint16_t *) pktbuf)[6]);
		nread += ic.offset;
		struct sk_buff s = {buffer, (size_t) nread, BUF_SIZE, etype};
		if (cb(&s) < 0) {
			perror("cb");
			break;
		}
	}
}


/* Add additional IPv6 header
 */
int ipv6_encap(struct sk_buff *buf, struct ip6_hdr *ip6)
{
	if (buf_reserve_space(buf, ETH_HLEN, 40) < 0)
		return -1;
	else if (buf_fill(buf, ETH_HLEN, ip6, 40) < 0)
		return -1;

	struct ip6_hdr *n = (struct ip6_hdr *) (buf->data + ETH_HLEN);
	n->ip6_plen = htons(buf->len - ETH_HLEN);
	n->ip6_nxt = 41;
	return 0;
}


ssize_t ipv6_extend_len(struct ip6_hdr *ip6, uint16_t sz)
{
	uint16_t v6len = ntohs(ip6->ip6_plen) + sz;
	ip6->ip6_plen = htons(v6len);
	return v6len;
}


// TODO: buf => sk_buff
void ipv6_srh_store(uint8_t *buf,
		uint8_t nexthdr,
		const uint8_t *segments, uint8_t num_segs,
		uint8_t *tlv_block, size_t tlv_len)
{
	// XXX tlv padding
	struct ip6_srh *srh = (struct ip6_srh *) buf;
	srh->nexthdr = nexthdr;
	srh->hdrlen = static_cast<uint8_t>(num_segs * 2 + tlv_len/8);
	srh->type = ROUTING_SRH;
	srh->segsleft = static_cast<uint8_t>(num_segs - 1);
	srh->last = static_cast<uint8_t>(num_segs - 1);
	srh->flags = 0;
	srh->tag = 0;

	if (segments)
		memcpy(buf + 8, segments, num_segs * 16);

	if (tlv_block && tlv_len)
		memcpy(buf + 8 + num_segs * 16, tlv_block, tlv_len);
}


void ipv6_srh_add(struct sk_buff *buf,
		const uint8_t *segments, uint8_t num_segs)
{
	const size_t offset = ETH_HLEN + IPV6_HLEN;
	uint8_t srhlen = 8 + 16 * num_segs;

	// if (buf->len + srhlen > buf->space) ...

	struct ip6_hdr *ip6 = (struct ip6_hdr *) (buf->data + ETH_HLEN);
	uint8_t nexthdr = ip6->ip6_nxt;

	buf_reserve_space(buf, offset, srhlen);

	ipv6_extend_len(ip6, srhlen);
	ip6->ip6_nxt = NEXTHDR_ROUTING;

	ipv6_srh_store(buf->data + offset, nexthdr,
			segments, num_segs, NULL, 0);
}


struct ip6_srh *ipv6_srh_valid(uint8_t *ptr, size_t tail_space)
{
	struct ip6_srh *srh = (struct ip6_srh *) ptr;

	ssize_t hsize = 8 + srh->hdrlen * 8;
	if (hsize > (ssize_t) tail_space) {
		fprintf(stderr, "Projected space %zd is larger than %zu available space\n",
				hsize, tail_space);
		return NULL;
	}

	if (srh->segsleft > srh->last) {
		fprintf(stderr, "Segment left %u is larger than %u available space\n",
				srh->segsleft, srh->last);
		return NULL;
	}

	ssize_t lsize = 8 + srh->last * 8;
	if (lsize > hsize) {
		fprintf(stderr, "Segment list %zd is larger than %zd available space\n",
				lsize, hsize);
		return NULL;
	}

#if 0
	// Untested
	ssize_t remain = hsize - lsize;
	const uint8_t *tlv = ptr + lsize;
	while (remain > 0) {
		if (remain < 2)
			return NULL;
		uint8_t vsize = tlv[1] + 2;
		if (vsize > remain)
			return NULL;
		remain -= vsize;
		tlv += vsize;
	}
#endif

	return srh;
}


void ipv6_srh_extend(struct sk_buff *buf,
	const uint8_t *segs,
	uint8_t num_segs,
	uint8_t flags)
{
	size_t srh_offset = ETH_HLEN + IPV6_HLEN;
	struct ip6_srh *srh = (struct ip6_srh *) (buf->data + srh_offset);

	uint8_t idx;
	if (flags == MUT_SRH_HERE) {
		idx = srh->segsleft;
	} else {
		idx = 0;
	}

	size_t seg_offset = srh_offset + sizeof(struct ip6_srh) +
			idx * 16;
	size_t added = num_segs * 16;

	buf_reserve_space(buf, seg_offset, added);
	buf_fill(buf, seg_offset, segs, added);

	if (flags == MUT_SRH_HERE)
		srh->segsleft += num_segs - 1;

	srh->last += num_segs;
	srh->hdrlen += num_segs * 2;

	struct ip6_hdr *ip6 = (struct ip6_hdr *) (buf->data + ETH_HLEN);
	ipv6_extend_len(ip6, added);
}


void l2_add(uint8_t *buf,
		const uint8_t *dst, const uint8_t *src, uint16_t etype)
{
	memcpy(buf, dst, 6);
	memcpy(buf + 6, src, 6);
	buf[12] = (etype >> 8) & 0xFF;
	buf[13] = etype & 0xFF;
}


void l2_swap(uint8_t *buf)
{
	uint8_t scratch[6];
	memcpy(scratch, buf, 6);
	memcpy(buf, buf + 6, 6);
	memcpy(buf + 6, scratch, 6);
}
