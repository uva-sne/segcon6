#pragma once

/* Utilities */

static void __attribute__((unused)) hexdump(const uint8_t *buf, ssize_t s)
{
	for (ssize_t i = 0; i < s; i++) {
		printf(" %02x", buf[i]);
		if ((i % 16) == 15) puts("");
	}
	printf("\n");
}

static inline
__uint128_t
__attribute__((unused))
_ip6_as_int128(const uint8_t *v)
{
	const __uint128_t *p = reinterpret_cast<const __uint128_t*>(v);
	return *p;
}

#define ip6_as_int128(x) _ip6_as_int128(reinterpret_cast<const uint8_t*>(x))

#define callf(fmt, ...) { \
	char scratch[1024]; \
	if (snprintf(scratch, sizeof(scratch), fmt, __VA_ARGS__) >= 1024) { \
		fprintf(stderr, "Command too big, bail\n"); \
		abort(); \
	} \
	if (system(scratch) != 0) { \
		perror("system"); \
		abort(); \
	} \
}

#define printv6(x) { \
	char scratch[64]; \
	inet_ntop(AF_INET6, reinterpret_cast<const uint8_t*>(x), scratch, 63); \
	scratch[63] = 0; \
	fprintf(stderr, "%s", scratch); }

#define printmac(x) { \
	fprintf(stderr, "%02x:%02x:%02x:%02x:%02x:%02x", x[0], x[1], x[2], x[3], x[4], x[5]); }

#ifdef VERBOSE
#define verbosef(...) fprintf(stderr, ##__VA_ARGS__)
#else
#define verbosef(...) {}
#endif
