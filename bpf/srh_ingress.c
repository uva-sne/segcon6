#include <stdint.h>
#include <stdlib.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/in6.h>
#include <linux/bpf.h>
#include "bpf_helpers.h"
#include "bpf_elf.h"


struct fwd_value {
	unsigned int ifindex;
	uint8_t mac[6];
} __attribute__((packed));


// Tenant SID + container IP
#define FWD_KEY_SIZE 32

// Allow per-interface maps
#ifndef FWD_MAP
#ifdef MULTI_TENANCY
#define FWD_MAP sc6_mt
#else
#define FWD_MAP sc6_fwd
#endif
#endif // FWD_MAP

// NB: key entries are swapped (SID list order)
struct bpf_elf_map SEC(ELF_SECTION_MAPS) FWD_MAP = {
	.type           = BPF_MAP_TYPE_HASH,
	.size_key       = FWD_KEY_SIZE,
	.size_value     = sizeof(struct fwd_value),
	.pinning        = PIN_GLOBAL_NS,
	.max_elem       = 128, // TODO
};


#define IPV6_HLEN 40
#define SRH_RTYPE_OFFSET 2
#define ROUTING_SRH 4

#define MAX_TLV_ENTRY 4

//#define REQUIRED_TLV 0x42,0x04,0xde,0xad,0xbe,0xef
#ifdef REQUIRED_TLV
static uint8_t required_tlv[] = {REQUIRED_TLV};
#endif


#define REQUIRED_SEGMENT 0xfd,0x04,0,0,0,0,0,0,0,0,0,0,0,0,0,1
#ifdef REQUIRED_SEGMENT
static uint8_t required_segment[16] = {REQUIRED_SEGMENT};
#endif

struct ip6_srh {
	uint8_t nexthdr;
	uint8_t hdr_ext_len;
	uint8_t rtype;
	uint8_t segs_left;
	uint8_t last;
	uint8_t flags;
	uint16_t tag;
	//void segments[0];
	//void tlv[0];
} __attribute__((packed));

//struct ip6_addr { uint32_t a, b, c, d; } __attribute__((packed));


#define v6match(left, right) \
	((left)[0] == (right)[0] && \
	 (left)[1] == (right)[1] && \
	 (left)[2] == (right)[2] && \
	 (left)[3] == (right)[3] && \
	 (left)[4] == (right)[4] && \
	 (left)[5] == (right)[5] && \
	 (left)[6] == (right)[6] && \
	 (left)[7] == (right)[7] && \
	 (left)[8] == (right)[8] && \
	 (left)[9] == (right)[9] && \
	 (left)[10] == (right)[10] && \
	 (left)[11] == (right)[11] && \
	 (left)[12] == (right)[12] && \
	 (left)[13] == (right)[13] && \
	 (left)[14] == (right)[14] && \
	 (left)[15] == (right)[15])


static __attribute__((always_inline)) int
is_link_local(uint8_t *addr)
{
	uint16_t *w = (uint16_t *) addr;
	return w[0] == 0xfe80;
}

#define COPY(dst, src, sz) \
	for (int i = 0; i < sz; i++) \
		(dst)[i] = (src)[i];

SEC("srh_ingress")
int do_srh_ingress(struct __sk_buff *skb)
{
//#define PTP_HACK
#ifdef PTP_HACK
	uint8_t broadcast[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	//uint8_t broadcast[6] = {0x00, 0x16, 0x3e, 0x40, 0xa0, 0xc6};
#endif

	int deliver_ifindex = -1;
	uint8_t *data = (void *) (long) skb->data;
	uint8_t *data_end = (void *) (long) skb->data_end;
	struct ip6_hdr *hdr;
	struct ip6_srh *srh;
	size_t hlen, sllen;
	uint8_t key[FWD_KEY_SIZE];

	const size_t ipv6_skip = ETH_HLEN + IPV6_HLEN;
#if defined(MULTI_TENANCY) || defined(REQUIRED_TLV)
	const size_t min_size = ipv6_skip + sizeof(struct ip6_srh);
#else
	const size_t min_size = ipv6_skip;
#endif

	// Must be IPv6
	if (unlikely(data + min_size > data_end))
		goto drop_invalid;
	else if (unlikely(data[12] != 0x86 && data[13] != 0xDD))
		goto pass_local;

	hdr = (struct ip6_hdr *) (data + ETH_HLEN);
	if (is_link_local(hdr->dst) || is_link_local(hdr->src))
		goto pass_local;

	// TODO: to prevent strange things from happening, you probably want to
	// verify that the current segment matches the destination address

	struct fwd_value *mtv = NULL;
	int have_key = 0;

#ifdef MULTI_TENANCY
	int activate_seg = 0;

	if (likely(hdr->nexthdr == IPPROTO_ROUTING)) {
		srh = (struct ip6_srh *) (data + ipv6_skip);
		// TODO: double check size field as well
		if (srh->segs_left > srh->last)
			goto drop_invalid;

		// Is this an overlay key?
		if (srh->segs_left == 1) {
			int offz = ETH_HLEN + IPV6_HLEN + sizeof(struct ip6_srh);
			if (unlikely(bpf_skb_load_bytes(skb, offz, key, 32) < 0)) {
				goto drop_invalid;
			}
			have_key = 1;

		}
	}
#endif

	if (unlikely(have_key)) {
		// Lookup single dst
		// TODO: we could have a different map for this
		int offz = ETH_HLEN + IPV6_HLEN - 16;
		if (unlikely(bpf_skb_load_bytes(skb, offz, key, 16) < 0)) {
			goto drop_invalid;
		}
		uint32_t *k = (uint32_t *) (key + 16);
		k[0] = 0; k[1] = 0; k[2] = 0; k[3] = 0;

		printk("srh_ingress: key is destination address\n");
	} else {
		printk("srh_ingress: key is segments\n");
	}

	mtv = bpf_map_lookup_elem(&FWD_MAP, key);
	if (unlikely(mtv == NULL)) {
		printk("srh_ingress: key lookup failed\n");
		goto drop_invalid;
	}

#ifdef MULTI_TENANCY
	if (activate_seg) {
		// Activate final segment
		if (unlikely(bpf_skb_store_bytes(skb, ETH_HLEN + IPV6_HLEN - 16,
						key, 16, 0) < 0)) {
			goto drop_invalid;
		}
		srh->segs_left = 0;
	}
#endif

	deliver_ifindex = mtv->ifindex;

#ifdef MULTI_TENANCY
	uint8_t mac[6];

	// TODO: this copy is not required in newer kernels, verifier bug
#pragma unroll
	COPY(mac, mtv->mac, 6);

	if (unlikely(bpf_skb_store_bytes(skb, 0, mac, 6, 0) < 0)) {
		return BPF_DROP;
	}
#endif

	if (unlikely(bpf_redirect(deliver_ifindex, 0) < 0)) {
		return BPF_DROP;
	}

	//bpf_skb_change_type(skb, PACKET_HOST);

	printk("srh_ingress: redirect %d\n", deliver_ifindex);
	return BPF_REDIRECT;

pass_local:
	//printk("srh_ingress: passing local traffic\n");
	return BPF_OK;

drop_invalid:
	printk("srh_ingress: rejecting packet\n");
	return BPF_DROP;
}

char _license[] SEC(ELF_SECTION_LICENSE) = "GPL";
