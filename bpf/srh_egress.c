/* This implementation is incomplete and this program is included as reference
 * only.
 */
#include <stdint.h>
#include <stdlib.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/in6.h>
#include <linux/bpf.h>
#include "bpf_helpers.h"
#include "bpf_elf.h"

// QA
#ifndef NO_DEFAULTS
#define OUT_MAP sc6_out
#define ADDR_MAP sc6_addr
// In bytes
#define TENANT_PREFIX 8
#define TENANT_SID_VALUE 0xDEAD
#define CONTAINER_ID 1
#define MAX_SEGMENTS 2

#endif

struct ip6_srh {
	uint8_t nexthdr;
	uint8_t hdr_ext_len;
	uint8_t rtype;
	uint8_t segs_left;
	uint8_t last;
	uint8_t flags;
	uint16_t tag;
	//void segments[0];
	//void tlv[0];
} __attribute__((packed));

struct segment {
	uint32_t seg[4];
} __attribute__((packed));

struct out_value {
	uint32_t ifindex;
	uint32_t tenant;
	// Per-container segs
	uint32_t nsegs;
	struct segment segs[MAX_SEGMENTS];
} __attribute__((packed));

struct bpf_elf_map SEC(ELF_SECTION_MAPS) OUT_MAP = {
	.type           = BPF_MAP_TYPE_HASH,
	.size_key       = sizeof(uint32_t),
	.size_value     = sizeof(struct out_value),
	.pinning        = PIN_GLOBAL_NS,
	.max_elem       = 128, // TODO
};


struct addrmap_value {
	uint8_t is_overlay;
	uint8_t underlay_prefix[TENANT_PREFIX];
} __attribute__((packed));

struct bpf_elf_map SEC(ELF_SECTION_MAPS) ADDR_MAP = {
	.type           = BPF_MAP_TYPE_HASH,
	.size_key       = TENANT_PREFIX,
	.size_value     = sizeof(struct addrmap_value),
	.pinning        = PIN_GLOBAL_NS,
	.max_elem       = 128, // TODO
};


// TODO: reject traffic with SRH already present
SEC("srh_egress")
int do_srh_egress(struct __sk_buff *skb) {
	uint8_t *data = (void *) (long) skb->data;
	uint8_t *data_end = (void *) (long) skb->data_end;
	if (unlikely(data + ETH_HLEN + 40 < data_end))
		return BPF_DROP;

	struct ip6_hdr *hdr = (struct ip6_hdr *) (data + ETH_HLEN);

#if 1 == 0
	// TODO: pass-through local traffic
	if (is_local_traffic(hdr))
		return BPF_OK;
#endif

	uint32_t ifindex = skb->ingress_ifindex;
	struct out_value *v = bpf_map_lookup_elem(&OUT_MAP, &ifindex);
	if (v == NULL)
		return BPF_DROP;

	int is_overlay = 0;
	uint8_t csegment[16];
	struct addrmap_value *a = bpf_map_lookup_elem(&ADDR_MAP, hdr->dst);

	if (a != NULL && a->is_overlay) {
		is_overlay = 1;

		// Add extra segment and do address translation
		uint32_t *c32 = (uint32_t *) csegment;
		uint32_t *u32 = (uint32_t *) a->underlay_prefix;
		// XXX hardcoded to /64
		c32[0] = u32[0];
		c32[1] = u32[1];
		c32[2] = TENANT_SID_VALUE;
		c32[3] = v->tenant;
	} else if (v->nsegs == 0) {
		// No SRH needed
		return BPF_OK;
	}

	uint8_t hlen = 8 + 16 * (1 + v->nsegs + (is_overlay ? 1 : 0));

	if (unlikely(bpf_skb_adjust_room(skb, hlen, BPF_ADJ_ROOM_NET, 0) < 0))
		return BPF_DROP;

	data = (void *) (long) skb->data;
	data_end = (void *) (long) skb->data_end;
	if (data + ETH_HLEN + 40 + hlen < data_end)
		return BPF_DROP;
	struct ip6_srh *srh = (struct ip6_srh *) (data + ETH_HLEN + 40);

	srh->nexthdr = data[ETH_HLEN + 7];
	data[ETH_HLEN + 7] = IPPROTO_ROUTING;
	/* TODO: fix ipv6 len? */

	srh->hdr_ext_len = v->nsegs * 2;
	srh->rtype = 4;
	srh->last = srh->segs_left = v->nsegs - 1;
	srh->flags = 0;
	srh->tag = 0;

	// Final segment
	if (unlikely(bpf_skb_store_bytes(skb, ETH_HLEN + 48, hdr->dst, 16, 0) < 0))
		return BPF_DROP;

	// Tenant cross-connect
	if (is_overlay) {
		if (unlikely(bpf_skb_store_bytes(skb, ETH_HLEN + 64, csegment, 16, 0) < 0))
			return BPF_DROP;
	}

	if (unlikely(v->nsegs > 0)) {
		int r = -1;
		int offz = ETH_HLEN + 48 + (is_overlay ? 32 : 16);
		// Sigh
#pragma unroll
		for (int i = 1; i < v->nsegs; i++) {
			if (i != v->nsegs) continue;
			r = bpf_skb_store_bytes(skb, ETH_HLEN + 48 + 32, v->segs, 16 * i, 0);
		}

		if (unlikely(r < 0))
			return BPF_DROP;

		//hdr->dst = last seg
	} else {
		//hdr->dst = last seg
	}

	return BPF_OK;
}

char _license[] SEC("license") = "GPL";
