/* Unconditionally redirect packets to another interface
 */
#include <stdint.h>
#include <stdlib.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/bpf.h>
#include "bpf_helpers.h"


SEC("redir")
int do_redir(struct __sk_buff *skb)
{
	uint8_t *data = (void *) (long) skb->data;
	uint8_t *data_end = (void *) (long) skb->data_end;

	if (unlikely(data + 3 > data_end))
		return BPF_DROP;
	else if (unlikely(data[0] == 0x33))
		return BPF_OK;

#ifndef NO_FUDGE_MAC
	uint8_t broadcast[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	if (unlikely(bpf_skb_store_bytes(skb, 0, broadcast, 6, 0) < 0)) {
		return BPF_DROP;
	}
#endif
	
#if 0
	// This was suggested by Daniel Borkmann, but does not work without
	// MAC adjustment -- TODO
	if (unlikely(bpf_skb_change_type(skb, PACKET_HOST) < 0)) {
		return BPF_DROP;
	}
#endif

	if (unlikely(bpf_redirect(TARGET_IFINDEX, 0) < 0)) {
		return BPF_DROP;
	}

	return BPF_REDIRECT;
}

char _license[] SEC("license") = "GPL";
