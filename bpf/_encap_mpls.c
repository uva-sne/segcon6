#include <uapi/linux/bpf.h>
#include "bpf_helpers.h"


#define ETH_HLEN 14
#define MPLS_LABEL 4


static inline int mpls_add_srh(struct __sk_buff *skb, uint32_t label)
{
    uint32_t l2sz = ETH_HLEN + MPLS_LABEL;
    uint8_t l2[l2sz];
    int r;

    if (unlikely((r = bpf_skb_change_head(skb, l2sz, 0)) < 0)) {
        printk("srh_inject: failed to change_head %d\n", r);
        return -1;
    }

    l2[0] = 0x00; l2[1] = 0x16; l2[2] = 0x3e; l2[3] = 0xb0; l2[4] = 0x39; l2[5] = 0xe8;
    l2[6] = 0x00; l2[7] = 0x16; l2[8] = 0x3e; l2[9] = 0x00; l2[10] = 0x00; l2[11] = 0x00;
    l2[12] = 0x88; l2[13] = 0x47;

    label = (label << 12) | 511;

    l2[14] = (label >> 24) & 0xFF;
    l2[15] = (label >> 16) & 0xFF;
    l2[16] = (label >>  8) & 0xFF;
    l2[17] = (label      ) & 0xFF;

    if (unlikely(bpf_skb_store_bytes(skb, 0, l2, l2sz, 0) < 0)) {
        printk("srh_inject: store L2 failed\n");
        return -1;
    }

    return BPF_OK;
}

SEC("srh_inject")
int do_srh_inject(struct __sk_buff *skb) {
    mpls_add_srh(skb, 101);
    return BPF_OK;
}

char _license[] SEC("license") = "GPL";
